$(document).ready(function() {
    // Obtener el ID del usuario desde la URL
    const urlParams = new URLSearchParams(window.location.search);
    const userId = urlParams.get('id');

    if (!userId) {
        alert('No se ha proporcionado un ID de usuario.');
        return;
    }

    // Función para obtener y llenar los datos del usuario en el formulario
    function fetchUserData() {
        $.ajax({
            url: localServer + "user/" + userId,
            method: "GET",
            dataType: 'json',
            success: (resp) => {
                if (resp) {
                    $('#nombre').val(resp.nombre);
                    $('#cedula').val(resp.cedula);
                    $('#membresia').val(resp.membresia);
                } else {
                    alert('No se encontraron datos para este usuario.');
                }
            },
            error: () => {
                alert('Error al obtener los datos del usuario.');
            }
        });
    }

    fetchUserData();

    // Manejar el envío del formulario para actualizar los datos del usuario
    $('form').on('submit', function(e) {
        e.preventDefault();
        
        const updatedUser = {
            nombre: $('#nombre').val(),
            cedula: $('#cedula').val(),
            membresia: $('#membresia').val()
        };

        $.ajax({
            url: localServer + "user/" + userId,
            method: "PUT",
            contentType: 'application/json',
            data: JSON.stringify(updatedUser),
            success: () => {
                alert('Usuario actualizado exitosamente.');
                window.location.href = 'index.html';
            },
            error: () => {
                alert('Error al actualizar los datos del usuario.');
            }
        });
    });
});

