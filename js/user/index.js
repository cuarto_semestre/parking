$(document).ready(function() {

    function fetchData() {
        $.ajax({
            url: localServer + "user/",
            method: "GET",
            dataType: 'Json',
            success: (resp) => {
                $("#tableBody").empty();
                if (resp.total > 0) {
                    resp.registros.map(res => {
                      $("#tableBody").append(`
                        <tr>
                          <td>${res.nombre}</td>
                          <td>${res.cedula}</td>
                          <td>${res.membresia == 1 ? 'Sí' : 'No'}</td>
                          <td>
                                <a href="edit.html?id=${res.id}" class="open-modal btn btn-warning btn-sm me-2" data-id="${res.id}">Editar</a>  
                          </td>

                        <tr>
                      `);
                    });
                  } else { alert(`No hay usuarios registrados actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }
    
    fetchData();

});