$(document).ready(function() {
    function handleSubmit(event) {
        event.preventDefault();

        if (document.getElementById('myForm').checkValidity()) {
            var formData = {
                nombre: $('#nombre').val(),
                cedula: $('#cedula').val(),
                membresia: $('#membresia').val()
            };

            $.ajax({
                url: localServer + "user/",
                method: 'POST',
                data: formData,
                success: function(resp) {
                    alert(resp.mensaje);
                    if (parseInt(resp.estado)) { location.href = "../../views/user/"; }
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    $('#myForm').on('submit', handleSubmit);
});