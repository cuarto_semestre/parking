$(document).ready(function() {
    function fetchData() {
        $.ajax({
            url: localServer + "vehiculo/",
            method: "GET",
            dataType: 'Json',
            success: (resp) => {
                $("#tableBody").empty();
                if (resp.total > 0) {
                    resp.registros.map(res => {
                      $("#tableBody").append(`
                        <tr>
                          <td>${res.placa}</td>
                          <td>${res.marca}</td>
                          <td>${res.tarifa_id == 1 ? 'Carro' : 'Moto'}</td>
                        <tr>
                      `);
                    });
                  } else { alert(`No hay vehículos actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }

    fetchData();
});
