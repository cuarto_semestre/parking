$(document).ready(function() {
    var selectedEmployeeId;

    function fetchEmployees() {
        var employees = JSON.parse(localStorage.getItem('employees')) || [];
        $("#tableBody").empty(); // Limpia cualquier contenido previo
        if (employees.length > 0) {
            employees.forEach(function(emp, index) {
                $("#tableBody").append(`
                    <tr>
                        <td>${emp.nombre}</td>
                        <td>${emp.direccion}</td>
                        <td>${emp.telefono}</td>
                        <td>
                            <a class="edit-employee btn btn-primary btn-sm me-2" data-id="${index}" data-nombre="${emp.nombre}" data-direccion="${emp.direccion}" data-telefono="${emp.telefono}">Editar</a>
                            <a class="delete-employee btn btn-danger btn-sm" data-id="${index}">Eliminar</a>
                        </td>
                    </tr>
                `);
            });
        } else {
            alert("No hay empleados actualmente.");
        }
    }

    function handleAdd(event) {
        event.preventDefault();

        var employees = JSON.parse(localStorage.getItem('employees')) || [];
        var formData = {
            nombre: $('#nombre').val(),
            direccion: $('#direccion').val(),
            telefono: $('#telefono').val()
        };

        employees.push(formData);
        localStorage.setItem('employees', JSON.stringify(employees));
        $('#formAdd')[0].reset();  // Reset form after submission
        fetchEmployees();
    }

    function handleUpdate(event) {
        event.preventDefault();

        var employees = JSON.parse(localStorage.getItem('employees')) || [];
        var formData = {
            nombre: $('#updateNombre').val(),
            direccion: $('#updateDireccion').val(),
            telefono: $('#updateTelefono').val()
        };

        employees[selectedEmployeeId] = formData;
        localStorage.setItem('employees', JSON.stringify(employees));
        $('#updateModal').modal('hide');
        fetchEmployees();
    }

    function handleDelete(event) {
        var employeeId = $(this).data('id');
        var employees = JSON.parse(localStorage.getItem('employees')) || [];

        employees.splice(employeeId, 1);
        localStorage.setItem('employees', JSON.stringify(employees));
        fetchEmployees();
    }

    fetchEmployees();

    $('#formAdd').on('submit', handleAdd);

    $(document).on('click', '.edit-employee', function() {
        selectedEmployeeId = $(this).data('id');
        $('#updateNombre').val($(this).data('nombre'));
        $('#updateDireccion').val($(this).data('direccion'));
        $('#updateTelefono').val($(this).data('telefono'));
        $('#updateModal').modal('show');
    });

    $('#formUpdate').on('submit', handleUpdate);

    $(document).on('click', '.delete-employee', handleDelete);
});
