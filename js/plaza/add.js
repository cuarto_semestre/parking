$(document).ready(function() {
    function handleSubmit(event) {
        event.preventDefault();

        if (document.getElementById('myForm').checkValidity()) {
            var formData = {
                numero: $('#numero').val(),
                planta: $('#planta').val(),
                tipo: $('#tipo').val()
            };

            $.ajax({
                url: localServer + "plaza/",
                method: 'POST',
                data: formData,
                success: function(resp) {
                    alert(resp.mensaje);
                    if (parseInt(resp.estado)) { location.href = "../../views/plaza/"; }
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    $('#myForm').on('submit', handleSubmit);
});
