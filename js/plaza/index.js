$(document).ready(function() {
    var selectedPlazaId;
    var selectedPlazaDisponible;

    function fetchData(planta = 1) {
        $.ajax({
            url: localServer + "plaza/",
            method: "GET",
            data: { planta },
            dataType: 'Json',
            success: (resp) => {
                $("#tableBody").empty();
                if (resp.total > 0) {
                    resp.registros.map(res => {
                      $("#tableBody").append(`
                        <tr>
                          <td>${res.numero}</td>
                          <td>${res.tipo == 1 ? 'Carro' : 'Moto'}</td>
                          <td>${
                            res.disponible ? 
                                `<a class="open-modal btn btn-primary btn-sm me-2" data-id="${res.id}" data-disponible="${res.disponible}">O</a>` :
                                `<a class="open-modal btn btn-danger btn-sm me-2" data-id="${res.id}" data-disponible="${res.disponible}">X</a>`
                            }
                          </td>
                        <tr>
                      `);
                    });
                  } else { alert(`No hay plazas en la planta ${planta} actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }

    function handleUpdate(event) {
        event.preventDefault();

        if (document.getElementById('formUpdate').checkValidity()) {
            var formDataPlazaVehiculo = {
                plaza_id: selectedPlazaId,
                vehiculo_id: $('#vehiculo_id').val()
            };

            var formDataPlaza = {
                id: selectedPlazaId,
                disponible: selectedPlazaDisponible == 1 ? 0 : 1
            };

            $.ajax({
                url: localServer + "plaza/",
                method: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(formDataPlaza),
                success: function(resp) {
                    alert(resp.mensaje);
                    if (parseInt(resp.estado)) {
                        if (selectedPlazaDisponible) {
                            $.ajax({
                                url: localServer + "plazaVehiculo/",
                                method: 'POST',
                                data: formDataPlazaVehiculo,
                                success: function(resp) {},
                                error: function(error) { console.error('Error submitting form:', error); }
                            });
                        }
                        location.href = "../../views/plaza/";
                    }
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    function fetchDataVehiculo() {
        $.ajax({
            url: localServer + "vehiculo/",
            method: "GET",
            dataType: 'Json',
            success: (resp) => {
                $("#vehiculo_id").empty();
                if (resp.total > 0) {
                    resp.registros.map(res => {
                        $("#vehiculo_id").append(`<option value="${res.id}">${res.placa}</option>`);
                    });
                } else { alert(`No hay plazas en la planta ${planta} actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }

    fetchData();

    $('#planta').change(function() {
        fetchData($(this).val());
    });

    $(document).on('click', '.open-modal', function() {
        selectedPlazaId = $(this).data('id');
        selectedPlazaDisponible = $(this).data('disponible');
        $("#modalBodyUpdatePlaza").empty();

        if (selectedPlazaDisponible) {
            $("#modalBodyUpdatePlaza").append(`<div class="form-group">
                <label for="vehiculo_id">Vehículo</label>
                <select class="form-control" name="vehiculo_id" id="vehiculo_id"></select>
            </div>`);
            fetchDataVehiculo();
        } else {
            $("#modalBodyUpdatePlaza").append(`<div class="form-group">
                <p>¿Esta seguro de poner esta plaza como disponible?</p>
            </div>`);
        }

        $('#updateModal').modal('show');
    });

    $('#formUpdate').on('submit', handleUpdate);
});
