$(document).ready(function() {
    function fetchData() {
        $.ajax({
            url: localServer + "tarifa/",
            method: "GET",
            dataType: 'Json',
            success: (resp) => {
                $("#tableBody").empty();
                if (resp.total > 0) {
                    resp.registros.map(res => {
                      $("#tableBody").append(`
                        <tr>
                            <td>${res.tipo_vehiculo == 1 ? 'Carro' : 'Moto'}</td>
                            <td>$ ${res.precio_hora}</td>
                        <tr>
                      `);
                    });
                  } else { alert(`No hay tarifas actualmente.`); }
            },
            error: () => { $.msgbox("Vuelva a intentarlo, no se ha creado el usuario"); }
        });
    }

    fetchData();
});
