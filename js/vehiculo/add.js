$(document).ready(function() {
    function handleSubmit(event) {
        event.preventDefault();

        if (document.getElementById('myForm').checkValidity()) {
            var formData = {
                placa: $('#placa').val(),
                marca: $('#marca').val(),
                tarifa_id: $('#tarifa_id').val()
            };

            $.ajax({
                url: localServer + "vehiculo/",
                method: 'POST',
                data: formData,
                success: function(resp) {
                    alert(resp.mensaje);
                    if (parseInt(resp.estado)) { location.href = "../../views/vehiculo/"; }
                },
                error: function(error) { console.error('Error submitting form:', error); }
            });
        } else { $('<input type="submit">').hide().appendTo('#myForm').click().remove(); }
    }

    $('#myForm').on('submit', handleSubmit);
});
