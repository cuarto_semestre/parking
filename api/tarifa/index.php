<?php
include '../config/header.php';
include_once '../config/database.php';
include_once '../entities/tarifa.php';

$db = new Database();
$connection = $db->getConnection();

$tarifa = new Tarifa($connection);

switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        $stmt = $tarifa->read();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $tarifas["registros"] = array();
            $tarifas["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    "id" => $id,
                    "tipo_vehiculo" => $tipo_vehiculo,
                    "precio_hora" => $precio_hora
                );
                array_push($tarifas["registros"], $u);
            }
            echo json_encode($tarifas);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
        $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"));

        $tarifa->tipo_vehiculo = $data->tipo_vehiculo;
        $tarifa->precio_hora = $data->precio_hora;

        $verify = $tarifa->validate();

        if ($verify["total"] == 0) {
            $status = $tarifa->create();
            if ($status == []) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "Tarifa creada correctamente."
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al crear la tarifa.",
                    "objeto":"'.$status[2].'"';
                echo '}';
            }
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al crear la tarifa '.$tarifa->numero.' porque ya existe."
                ';
            echo '}';
        }

        break;

    case "PUT":
        // $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"), true);

        $input = file_get_contents('php://input');
        $data = json_decode($input, true);

        $tarifa->id = $data["id"];
        $tarifa->precio_hora = $data["precio_hora"];

        if ($tarifa->update()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Tarifa actualizada correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al actualizar la tarifa."';
            echo '}';
        }
        break;

    case "DELETE":
        $data = json_decode(json_encode($_POST));

        $tarifa->id = $data->id;

        if ($tarifa->delete()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Usuario elimminado correctamente"
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al eliminar el usuario"';
            echo '}';
        }
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
