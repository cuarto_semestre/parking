<?php
include '../config/header.php';
include_once '../config/database.php';
include_once '../entities/plazaVehiculo.php';

$db = new Database();
$connection = $db->getConnection();

$plazaVehiculo = new PlazaVehiculo($connection);

switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        $stmt = $plazaVehiculo->read();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $plazaVehiculos["registros"] = array();
            $plazaVehiculos["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    
                );
                array_push($plazaVehiculos["registros"], $u);
            }
            echo json_encode($plazaVehiculos);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
        $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"));

        $plazaVehiculo->plaza_id = $data->plaza_id;
        $plazaVehiculo->vehiculo_id = $data->vehiculo_id;

        $status = $plazaVehiculo->create();
        if ($status == []) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Plaza creada correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al crear la planta.",
                "objeto":"'.$status[2].'"';
            echo '}';
        }

        break;

    case "PUT":
        // $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"), true);

        $input = file_get_contents('php://input');
        $data = json_decode($input, true);

        $plazaVehiculo->id = $data["id"];
        $plazaVehiculo->disponible = $data["disponible"];

        if ($plazaVehiculo->update()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Plaza actualizada correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al actualizar la plazaVehiculo."';
            echo '}';
        }
        break;

    case "DELETE":
        $data = json_decode(json_encode($_POST));

        $plazaVehiculo->id = $data->id;

        if ($plazaVehiculo->delete()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Usuario elimminado correctamente"
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al eliminar el usuario"';
            echo '}';
        }
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
