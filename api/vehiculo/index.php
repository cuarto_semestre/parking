<?php
include '../config/header.php';
include_once '../config/database.php';
include_once '../entities/vehiculo.php';

$db = new Database();
$connection = $db->getConnection();

$vehiculo = new Vehiculo($connection);

switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        $stmt = $vehiculo->read();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $vehiculos["registros"] = array();
            $vehiculos["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    "id" => $id,
                    "placa" => $placa,
                    "marca" => $marca,
                    "tarifa_id" => $tarifa_id
                );
                array_push($vehiculos["registros"], $u);
            }
            echo json_encode($vehiculos);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
        $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"));

        $vehiculo->placa = $data->placa;
        $vehiculo->marca = $data->marca;
        $vehiculo->tarifa_id = $data->tarifa_id;

        $verify = $vehiculo->validate();

        if ($verify["total"] == 0) {
            $status = $vehiculo->create();
            if ($status == []) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "Vehículo creado correctamente."
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al crear el vehículo.",
                    "objeto":"'.$status[2].'"';
                echo '}';
            }
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al crear el vehículo '.$vehiculo->placa.' porque ya existe."
                ';
            echo '}';
        }

        break;

    case "PUT":
        // $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"), true);

        $input = file_get_contents('php://input');
        $data = json_decode($input, true);

        $vehiculo->id = $data["id"];
        $vehiculo->disponible = $data["disponible"];

        if ($vehiculo->update()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Plaza actualizada correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al actualizar la vehiculo."';
            echo '}';
        }
        break;

    case "DELETE":
        $data = json_decode(json_encode($_POST));

        $vehiculo->id = $data->id;

        if ($vehiculo->delete()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Usuario elimminado correctamente"
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al eliminar el usuario"';
            echo '}';
        }
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
