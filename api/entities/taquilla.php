<?php
class Taquilla {

    // database connection and table name
    private $conn;
    private $table_name = "taquilla";

    // object properties
    public $id;
    public $direccion;
    public $nombre_empleado;
    public $telefono_empleado;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            direccion,
            nombre_empleado,
            telefono_empleado
        )
        VALUES
        (
            ?,
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->direccion);
        $stmt->bindParam(2, $this->nombre_empleado);
        $stmt->bindParam(3, $this->telefono_empleado);

        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET ";
        $query.= "direccion'".$this->direccion;
        $query.= "nombre_empleado'".$this->nombre_empleado:
        $query.= "telefono_empleado'".$this->telefono_empleado."' WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
