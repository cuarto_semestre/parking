<?php
class Facturas {

    // database connection and table name
    private $conn;
    private $table_name = "facturas";

    // object properties
    public $id;
    public $fecha_llegada;
    public $fecha_salida;
    public $codigo;
    public $metodo_pago;
    public $vehiculo_id;
    public $usuario_id;
    public $taquilla_id;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            fecha_llegada,
            fecha_salida,
            codigo,
            metodo_pago,
            vehiculo_id,
            usuario_id,
            taquilla_id
        )
        VALUES
        (
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->fecha_llegada);
        $stmt->bindParam(2, $this->fecha_salida);
        $stmt->bindParam(3, $this->codigo);
        $stmt->bindParam(4, $this->metodo_pago);
        $stmt->bindParam(5, $this->vehiculo_id);
        $stmt->bindParam(6, $this->usuario_id);
        $stmt->bindParam(7, $this->taquilla_id);

        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET ";
        $query.= "tipo_vehiculo='".$this->tipo_vehiculo;
        $query.= "precio_hora='".$this->precio_hora."' WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
