<?php
class Plaza {

    // database connection and table name
    private $conn;
    private $table_name = "plaza";

    // object properties
    public $id;
    public $numero;
    public $planta;
    public $tipo;
    public $disponible;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            numero,
            planta,
            tipo
        )
        VALUES
        (
            ?,
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->numero);
        $stmt->bindParam(2, $this->planta);
        $stmt->bindParam(3, $this->tipo);

        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT * FROM ".$this->table_name." WHERE planta=$this->planta";

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET ";
        $query.= "disponible='".$this->disponible."' WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function validate() {
        $stmt = $this->conn->prepare("SELECT COUNT(*) total FROM ".$this->table_name." WHERE numero='$this->numero' AND planta='$this->planta'");
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
