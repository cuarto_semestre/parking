<?php
class User {

    // database connection and table name
    private $conn;
    private $table_name = "usuario";

    // object properties
    public $id;
    public $nombre;
    public $cedula;
    public $membresia;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            id,
            nombre,
            cedula,
            membresia
        )
        VALUES
        (
            ?,
            ?,
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->bindParam(2, $this->nombre);
        $stmt->bindParam(3, $this->cedula);
        $stmt->bindParam(4, $this->membresia);
        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET 
        id='".$this->id."',
        nombre='".$this->nombre."',
        cedula='".$this->cedula."',
        membresia='".$this->membresia."'

        WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function validate() {
        $stmt = $this->conn->prepare("SELECT COUNT(*) total FROM ".$this->table_name." WHERE nombre='".$this->nombre."'");
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function closeConnection() {
        $this->conn = null;
    }
}

