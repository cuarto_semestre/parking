<?php
class PlazaVehiculo {

    // database connection and table name
    private $conn;
    private $table_name = "plaza_has_vehiculo";

    // object properties
    public $plaza_id;
    public $vehiculo_id;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            plaza_id,
            vehiculo_id
        )
        VALUES
        (
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->plaza_id);
        $stmt->bindParam(2, $this->vehiculo_id);

        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET ";
        $query.= "plaza_id='".$this->plaza_id;
        $query.= "vehiculo_id='".$this->vehiculo_id."' WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
