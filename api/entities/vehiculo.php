<?php
class Vehiculo {

    // database connection and table name
    private $conn;
    private $table_name = "vehiculo";

    // object properties
    public $id;
    public $placa;
    public $marca;
    public $tarifa_id;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            placa,
            marca,
            tarifa_id
        )
        VALUES
        (
            ?,
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->placa);
        $stmt->bindParam(2, $this->marca);
        $stmt->bindParam(3, $this->tarifa_id);

        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET ";
        $query.= "placa='".$this->placa;
        $query.= "marca='".$this->marca."' WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function validate() {
        $stmt = $this->conn->prepare("SELECT COUNT(*) total FROM ".$this->table_name." WHERE placa='$this->placa'");
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
