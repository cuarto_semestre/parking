<?php
class Tarifa {

    // database connection and table name
    private $conn;
    private $table_name = "tarifa";

    // object properties
    public $id;
    public $tipo_vehiculo;
    public $precio_hora;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    //C
    public function create() {
        $query = "INSERT INTO ".$this->table_name." 
        (
            tipo_vehiculo,
            precio_hora
        )
        VALUES
        (
            ?,
            ?
        )";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->tipo_vehiculo);
        $stmt->bindParam(2, $this->precio_hora);

        if (!$stmt->execute()) { return $stmt->errorInfo(); }
        else { return []; }
    }

    //R
    public function read() {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        $query = "UPDATE ".$this->table_name." SET ";
        $query.= "tipo_vehiculo='".$this->tipo_vehiculo;
        $query.= "precio_hora='".$this->precio_hora."' WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    //D
    public function delete() {
        $query = "DELETE FROM ".$this->table_name." WHERE id=".$this->id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function validate() {
        $stmt = $this->conn->prepare("SELECT COUNT(*) total FROM ".$this->table_name." WHERE tipo_vehiculo='$this->tipo_vehiculo'");
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
