<?php
include '../config/header.php';
include_once '../config/database.php';
include_once '../entities/plaza.php';

$db = new Database();
$connection = $db->getConnection();

$plaza = new Plaza($connection);

switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        $plaza->planta = $_GET['planta'];
        $stmt = $plaza->read();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $plazas["registros"] = array();
            $plazas["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    "id" => $id,
                    "numero" => $numero,
                    "tipo" => $tipo,
                    "disponible" => $disponible
                );
                array_push($plazas["registros"], $u);
            }
            echo json_encode($plazas);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
        $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"));

        $plaza->numero = $data->numero;
        $plaza->planta = $data->planta;
        $plaza->tipo = $data->tipo;

        $verify = $plaza->validate();

        if ($verify["total"] == 0) {
            $status = $plaza->create();
            if ($status == []) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "Plaza creada correctamente."
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al crear la planta.",
                    "objeto":"'.$status[2].'"';
                echo '}';
            }
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al crear planta '.$plaza->numero.' porque ya existe."
                ';
            echo '}';
        }

        break;

    case "PUT":
        // $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"), true);

        $input = file_get_contents('php://input');
        $data = json_decode($input, true);

        $plaza->id = $data["id"];
        $plaza->disponible = $data["disponible"];

        if ($plaza->update()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Plaza actualizada correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al actualizar la plaza."';
            echo '}';
        }
        break;

    case "DELETE":
        $data = json_decode(json_encode($_POST));

        $plaza->id = $data->id;

        if ($plaza->delete()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Usuario elimminado correctamente"
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al eliminar el usuario"';
            echo '}';
        }
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
