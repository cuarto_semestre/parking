<?php
include '../config/header.php';
include_once '../config/database.php';
include_once '../entities/facturas.php';

$db = new Database();
$connection = $db->getConnection();

$factura = new Facturas($connection);

switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
        $stmt = $factura->read();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $facturas["registros"] = array();
            $facturas["total"] = $count;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $u  = array(
                    "id" => $id,
                    "fecha_llegada" => $fecha_llegada,
                    "fecha_salida" => $fecha_salida,
                    "codigo" => $codigo,
                    "metodo_pago" => $metodo_pago,
                    "vehiculo_id" => $vehiculo_id,
                    "usuario_id" => $usuario_id,
                    "taquilla_id" => $taquilla_id
                );
                array_push($facturas["registros"], $u);
            }
            echo json_encode($facturas);
        }
        else { echo json_encode(array("registros" => array(), "total" => 0)); }
        break;

    case "POST":
        $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"));

        $factura->fecha_llegada = $data->fecha_llegada;
        $factura->fecha_salida = $data->fecha_salida;
        $factura->codigo = $data->codigo;
        $factura->metodo_pago = $data->metodo_pago;
        $factura->vehiculo_id = $data->vehiculo_id;
        $factura->usuario_id = $data->usuario_id;
        $factura->taquilla_id = $data->taquilla_id;

        if ($verify["total"] == 0) {
            $status = $factura->create();
            if ($status == []) {
                echo '{';
                    echo '
                    "estado": 1,
                    "mensaje": "factura creada correctamente."
                    ';
                echo '}';
            } else {
                echo '{';
                    echo '
                    "estado": 0,
                    "mensaje": "Error al crear la factura.",
                    "objeto":"'.$status[2].'"';
                echo '}';
            }
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al crear la factura '.$factura->numero.' porque ya existe."
                ';
            echo '}';
        }

        break;

    case "PUT":
        // $data = json_decode(json_encode($_POST));
        // $data = json_decode(file_get_contents("php://input"), true);

        $input = file_get_contents('php://input');
        $data = json_decode($input, true);

        $factura->id = $data["id"];
        $factura->precio_hora = $data["precio_hora"];

        if ($factura->update()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "factura actualizada correctamente."
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al actualizar la factura."';
            echo '}';
        }
        break;

    case "DELETE":
        $data = json_decode(json_encode($_POST));

        $factura->id = $data->id;

        if ($factura->delete()) {
            echo '{';
                echo '
                "estado": 1,
                "mensaje": "Usuario elimminado correctamente"
                ';
            echo '}';
        } else {
            echo '{';
                echo '
                "estado": 0,
                "mensaje": "Error al eliminar el usuario"';
            echo '}';
        }
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        exit();
}
?>
